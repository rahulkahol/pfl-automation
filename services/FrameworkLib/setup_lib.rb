####PFL Automation #########################################
require 'capybara'
require 'capybara/dsl'
require 'rspec/core'
require 'capybara/rspec/matchers'
require 'capybara/rspec/features'
require "selenium-webdriver"
require 'capybara/rspec'
require 'capybara-screenshot'
require 'capybara-screenshot/rspec'
require './services/FrameworkLib/utility_lib.rb'

#Connstant variables to be used.
$downloadFilePath ='/downloadfiles/'
$APP_URL = "https://login.salesforce.com"
$window_os='Windows'
$mac_os='Mac'
OSTYPE = ENV['OSTYPE'] ? ENV['OSTYPE'] : UTIL.get_property_value("os_type")
DRIVER = ENV['DRIVER'] ? ENV['DRIVER'] : UTIL.get_property_value("driver")

#Register different drivers with Capybara
Capybara.register_driver :chrome do |app|
	Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

Capybara.register_driver :safari do |app|
	Capybara::Selenium::Driver.new(app, :browser => :safari)
end

Capybara.register_driver :internet_explorer do |app|
	Capybara::Selenium::Driver.new(app, :browser => :internet_explorer)
end

Capybara::Screenshot.register_driver(:chrome) do |driver, path|
 driver.browser.save_screenshot(path)
end

#Browser profile
FIREFOX_PROFILE1 = "firefox_profile1"
#firefox Profile 1
begin
	$firefox_profile1 = Selenium::WebDriver::Firefox::Profile.new
	if OSTYPE == $window_os
		file_path = Dir.pwd+"\\testDownloadFiles\\"
		filepath = file_path.gsub("/", "\\")
	else
		filepath = Dir.pwd+ $downloadFilePath
	end

	$firefox_profile1['browser.download.folderList'] = 2
	$firefox_profile1['extensions.fnvfox.general.downloads.downloadsFolder'] = filepath
	$firefox_profile1['browser.download.dir'] = filepath
	$firefox_profile1['browser.download.downloadDir']=filepath
	$firefox_profile1['browser.download.lastDir']=filepath
	$firefox_profile1['browser.download.dir']=filepath
	$firefox_profile1['browser.download.defaultFolder']=filepath
	$firefox_profile1['browser.download.panel.shown'] = false
	$firefox_profile1['browser.helperApps.alwaysAsk.force'] = false
	$firefox_profile1['browser.download.manager.showWhenStarting'] = false
	$firefox_profile1['browser.download.manager.alertOnEXEOpen'] = false
	$firefox_profile1['browser.helperApps.neverAsk.saveToDisk'] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet , application/csv , application/json , application/xls , application/pdf, application/html, application/vnd.ms-excel, application/htm, application/octet-stream, text/csv"
end

if DRIVER == "firefox"
	Capybara.register_driver :selenium_custom do |app|
		Capybara::Selenium::Driver.new(app, :browser => :firefox, :profile => $firefox_profile1)
	end
	Capybara::Screenshot.register_driver(:selenium_custom) do |driver, path|
		driver.browser.save_screenshot path
	end
	Capybara.default_driver = :selenium_custom
elsif DRIVER == "chrome"
	Capybara.default_driver = :chrome

elsif DRIVER == "safari"
	Capybara.default_driver = :safari

elsif DRIVER == "internet_explorer"
	Capybara.default_driver = :internet_explorer
end

unless DRIVER == "firefox" || DRIVER == "chrome" || DRIVER == "safari" || DRIVER == "internet_explorer" || DRIVER == "SAUCE"
  raise "You must or DRIVER=chrome or DRIVER=safari or DRIVER=firefox or DRIVER=internet_explorer to the beginning of your command"
end

$supported_pdf_drivers = ["firefox"]

Capybara.configure do |con|
	con.app_host = $APP_URL
	con.ignore_hidden_elements = true
	con.app_host = @url
	con.run_server = false
	con.match =:prefer_exact
	con.default_max_wait_time = 10 # the default time is 2 seconds
	if (DRIVER == "firefox" || DRIVER == "chrome" || DRIVER == "safari" || DRIVER == "internet_explorer")
		con.current_session.driver.browser.manage.window.maximize
	end
end

RSpec.configure do |con|
	con.include Capybara::DSL, :type => :request
	con.include Capybara::DSL, :type => :acceptance
	con.include Capybara::RSpecMatchers, :type => :request
	con.include Capybara::RSpecMatchers, :type => :acceptance
	con.after do
		Capybara.use_default_driver
	end
end
#Set Current OS
$current_os=$window_os

if (OSTYPE==$window_os)
	$current_os=$window_os
else
	$current_os=$mac_os
end