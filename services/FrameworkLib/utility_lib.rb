### PFL Automation #########################
require 'capybara'
require 'capybara/dsl'
require 'rspec/core'
require 'capybara/rspec/matchers'
require 'capybara/rspec/features'
require "selenium-webdriver"
require 'capybara/rspec' 

extend RSpec::Matchers

module UTIL
extend Capybara::DSL
$gen_default_property_file_name="uitest.run.properties"
$object_wait_loop_iteration = 15
PROPERTY_FILE = ENV['PROPERTY_FILE'] ? ENV['PROPERTY_FILE'] : $gen_default_property_file_name 

# Press tab out on exsisting element
# this method will take element locator as Xpath or Css
def UTIL.tab_out element
	puts "testing"
end

# Method to compare two values exactly
# this method will take two values to compare and one as a message to display as a test message.
# PAssed or failed message would be displayed
def UTIL.compare_value expectedValue , actual_value , tstMsg
	if (expect(actual_value).to eql(expectedValue))
		puts "Comparison: #{tstMsg} : Passed."
	else
		raise "Comparison: #{tstMsg} : Failed."
	end 
end

# Method to check actual value contains expected value.
# return true/false as execution status.
def UTIL.include expectedValue , actualValue , tstMsg
	if (expect(actualValue).to include(expectedValue))
		puts "Test Result: #{tstMsg} : Passed."
	else
		raise "Test Result: #{tstMsg} : Failed."
	end 
end

# compare casse insensitive for strings
def UTIL.compare_ignore_case expectedValue , actualValue , tstMsg
	if expectedValue.is_a?(String)
		expectedValue = expectedValue.downcase
		actualValue = actualValue.downcase
	end 
	if (expect(actualValue).to eql(expectedValue))
		puts "Test #{sTest} Passed."
	end 
end

# Expected text is not included in the Actual text 
#return true/false as execution status
def UTIL.not_include expectedValue , actualValue , tstMsg
	
end

def UTIL.page_load_waiting
	begin
		page.has_css?($app_button)
	rescue
		UTIL.min_wait
	end
end

def UTIL.min_wait
	sleep $GEN_MIN_WAIT
end

# click on a link and wait
def UTIL.click_link_and_wait link_name
	click_link link_name
	UTIL.min_wait
end 

def UTIL.click_link link_name
	click_link link_name
end 

# read and return property value from uitest.run.properties
# Method will take property value as an parameter and return text as an value.
def UTIL.get_property_value prop_name
	property = ""
	File.open(PROPERTY_FILE, "r") do |infile|
		while (row = infile.gets)
			if row.include? prop_name
				property_row = row
			end 
		end
		property = property_row.split("=")
	end
	return  property[1].chop
end

# Method for Dynamic wait ,will take element_locator(object) as an parameter for which it will wait. 
#wait for element to disappear 
	def UTIL.wait_until_object_disappear object_path
		
	end	
	
	def UTIL.wait_object_appear object_path
		found_status = false
		for i in 1..$object_wait_loop_iteration do
			begin
				if(object_path[0,1]=='/')
					find(:xpath, object_path)
				else
					find(object_path)
				end
				found_status = true
				break
			rescue
				puts "Waiting for object to appear"
				UTIL.min_wait
			end
		end
		if(found_status==false)
			raise "Finally Object not found"
		end
	end	

# open link in new tab
	def UTIL.open_link_in_new_tab link_name
		
	end

	# to scroll to a particular element 
	def UTIL.scroll_to obj_locator
		locator_type = :css
		if(obj_locator[0]=='/'  || obj_locator[0,2] == '(/')
		locator_type = :xpath
		end
		script = <<-JS
		arguments[0].scrollIntoView(true);
		JS
		element_position = find(locator_type, obj_locator).native
		Capybara.current_session.driver.browser.execute_script(script, element_position)
	end

	
	#Method will destroy new data created for each test case	
	#Method will call once after each test case execution.
	def UTIL.destroy_new_created_data
	
	end
	
	#Method will hold the state of base data before execution of each test case
	#Method will call once before each test case execution.
	def UTIL.hold_base_data
		
	end
	
	
	# This method use for verify hidden elements on UI. Bydefault it is a True. But in some Test cases we required it False.
	def UTIL.ignore_hidden_elements bool_value
		
	end
	
	#Method to refresh the page
	def UTIL.page_refresh
		UTIL.press_keys_on_page 		
	end
	
	# parameter would be passed like :f1,  :f2,  :f3 etc
	# Don't pass parameter as a string
	def UTIL.press_keys_on_page key_value
		pageAction = page.driver.browser.action
		pageAction.send_keys(key_value).perform
		UTIL.min_wait
	end
############################################
#process_block
############################################
#Method used to rexecute code multiple times
	def UTIL.rexecute_script_block (&block)
		success_status=false
		exp = nil
		for i in 1..$SCRIPT_REXECUTION_COUNT do
			begin
				block.call()
			rescue Exception=>e
				puts "Script rexecution failed in #{i} attempt with message #{e}"
				exp = e
				UTIL.min_wait
			else
				success_status=true
				break
			end
		end
		if(success_status ==false)
			puts "Script rexecution failed in all attempt"
			raise exp
		end
	end
end