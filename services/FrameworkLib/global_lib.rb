require './services/FrameworkLib/utility_lib.rb'

#Connstant variables to be used 

#Set general variable values through enviormental variables or property file.
$SALESFORCE_URL = "https://login.salesforce.com"
$TEST_EXE_RESULT_FILE = "TestExecutionResult.txt"
$SCRIPT_REXECUTION_COUNT = 3
$GEN_MIN_WAIT = 5
$SALESFORCE_USERNAME = "username"
$SALESFORCE_PASSWORD = "password"

SFUSER = ENV['SFUSER'] ? ENV['SFUSER'] : UTIL.get_property_value("sfuser")
SFPASS = ENV['SFPASS'] ? ENV['SFPASS'] : UTIL.get_property_value("sfpass")  
RESULT_FILE = ENV['RESULT_FILE'] ? ENV['RESULT_FILE'] : $TEST_EXE_RESULT_FILE
MAIL_RECIPIENT = ENV['MAIL_RECIPIENT'] ? ENV['MAIL_RECIPIENT'] : UTIL.get_property_value("mail.recipient")
USER_MAIL = ENV['NOTIFY_USER'] ? ENV['NOTIFY_USER'] : UTIL.get_property_value("mail.user_name") 
USER_PASSWORD = ENV['NOTIFY_PASSWORD'] ? ENV['NOTIFY_PASSWORD'] : UTIL.get_property_value("mail.user_password")

def login_domain
	visit $SALESFORCE_URL
	fill_in $SALESFORCE_USERNAME, with: SFUSER
	fill_in $SALESFORCE_PASSWORD, with: SFPASS
	click_button "Login"
	if(page.has_xpath?($close_lighting_option))
		find(:xpath, $close_lighting_option).click
	end
	UTIL.page_load_waiting
end

shared_context "Login_Domain_Before_All" do
	before :all do
		login_domain
	end	
end

shared_context "Login_Domain_Before_Each" do
	before :each do
		login_domain
	end
end

shared_context "Logout_Domain_after_each" do
	after :each do 

	end
end




